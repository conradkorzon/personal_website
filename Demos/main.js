// const TTT_Status = document.getElementById("TTT-Label");
// var TTT_Continue = true;
// var TTT_Player = "O"

// let gameActive = true;

// let currentPlayer = "X";

// let gameState = ["", "", "", "", "", "", "", "", ""];

// const winningConditions = [
//     [0, 1, 2],
//     [3, 4, 5],
//     [6, 7, 8],
//     [0, 3, 6],
//     [1, 4, 7],
//     [2, 5, 8],
//     [0, 4, 8],
//     [2, 4, 6]
// ];

// const winningMessage = () => `Player ${currentPlayer} has won!`;

// const drawMessage = () => `Game ended in a draw!`;

// const currentPlayerTurn = () => `It's ${currentPlayer}'s turn`;

// statusDisplay.innerHTML = currentPlayerTurn();

// function handleCellPlayed(clickedCell, clickedCellIndex){
//     gameState[clickedCellIndex] = currentPlayer;
//     clickedCell.innerHTML = currentPlayer;
// }

// function handlePlayerChange(){
//     currentPlayer = currentPlayer === "X" ? "O" : "X";
//     statusDisplay.innerHTML = currentPlayerTurn();
// }

// function handleResultValidation(){
//     let roundWon = false;
//     for (let i = 0; i <= 7; i++){
//         const winCondition = winningConditions[i];
//         let a = gameState[winCondition[0]];
//         let b = gameState[winCondition[1]];
//         let c = gameState[winCondition[2]];
//         if (a === '' || b === '' || c === ''){
//             continue;
//         }
//         if (a === b && b === c) {
//             roundWon = true;
//             break
//         }
//     }
//     if (roundWon){
//         statusDisplay.innerHTML = winningMessage();
//         gameActive = false;
//         return;
//     }
//     let roundDraw = !gameState.includes("");
//     if (roundDraw) {
//         statusDisplay.innerHTML = drawMessage();
//         gameActive = false;
//         return;
//     }
//     handlePlayerChange();
// }

// function handleCellClick(clickedCellEvent){
//     const clickedCell = clickedCellEvent.target;
//     const clickedCellIndex = parseInt(clickedCell.getAttribute('data-TTT-cell'));
//     if (gameState[clickedCellIndex] !== "" || !gameActive) {
//         return;
//     }
//     handleCellPlayed(clickedCell, clickedCellIndex);
//     handleResultValidation();
// }

// function handleRestartGame(){
//     gameActive = true;
//     currentPlayer = "X";
//     gameState = ["", "", "", "", "", "", "", "", ""];
//     statusDisplay.innerHTML = currentPlayerTurn();
//     document.querySelectorAll('.TTT-box').forEach(cell => cell.innerHTML = "");
// }

// document.querySelectorAll('.TTT-box').forEach(cell => cell.addEventListener('click', handleCellClick));

// document.querySelector('.TTT-restart').addEventListener('click', handleRestartGame);

var players_turn = true;

var TTT_board = {
    1:"",
    2:"",
    3:"",
    4:"",
    5:"",
    6:"",
    7:"",
    8:"",
    9:""
};
var turn = 0;
var TTT_Status = document.getElementById("TTT-Label");
var player_symbol = "X";
var TTT_active = true;
var bot_symbol = "O";

function TTT_box_clicked(box_num){
    if(TTT_board[box_num]!="" || !players_turn){
        return;
    }
    players_turn = false;
    box = document.getElementById("TTT"+String(box_num));
    box.innerHTML= player_symbol;
    TTT_board[box_num] = player_symbol;
    turn+=1;
    win = TTT_check_win(player_symbol);
    if(win){
        handle_win(player_symbol);
        return;
    }
    
    TTT_bot_turn(box_num);
}

function TTT_bot_turn(player_choice){
    if(!TTT_active){
        return;
    }
    //TTT_Status.innerHTML = "Computer's Turn"
    document.getElementById("TTT-Label").innerHTML = "computer's turn";
    var options = [];
    [player_choice+1,player_choice-1,player_choice-3,player_choice+3].forEach((option) => {
        if(option > 0 && option < 10){
            if(TTT_board[option]==""){
                options.push(option);
            }
        }
    });
    if(options.length==0){
        for (const box in TTT_board){
            if(TTT_board[box]==""){
                options.push(box);
            }
        }
    }
    if(options.length<1){
        //player has won
        return
    }
    var choice = options[Math.floor(Math.random() * options.length)];
    TTT_board[choice] = bot_symbol;
    //console.log(choice);
    var box = document.getElementById("TTT"+String(choice));
    //console.log(box);
    box.innerHTML = bot_symbol;
    var win = TTT_check_win(bot_symbol);
    if(win){
        handle_win(bot_symbol);
    }
    else{
        players_turn = true;
        document.getElementById("TTT-Label").innerHTML = "Player's Turn";
    }
    return;
}

function handle_win(player){
    document.getElementById("TTT-Label").innerHTML = "Player "+player+" has won!";
    TTT_active = false;

}

const winningConditions = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 5, 9],
    [3, 5, 7]
];

function TTT_check_win(symbol){
    if (turn>2){
        for(i=0; i<winningConditions.length ; i++){
            if(TTT_board[winningConditions[i][0]]==symbol && TTT_board[winningConditions[i][1]]==symbol && TTT_board[winningConditions[i][2]]==symbol){
                return true;
            }
        }
    }
    return false;
}

function TTT_Restart(){
    TTT_active = true;
    players_turn = true;
    document.querySelectorAll('.TTT-box').forEach(box => box.innerHTML = "");
    turn = 0;
    TTT_board = {
        1:"",
        2:"",
        3:"",
        4:"",
        5:"",
        6:"",
        7:"",
        8:"",
        9:""
    };
}